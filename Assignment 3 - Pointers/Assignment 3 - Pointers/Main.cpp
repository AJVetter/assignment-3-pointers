
// Assignment 3 - Pointers
// AJ Vetter


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function

void SwapIntegers(int* firstVal, int* secondVal)
{
	// save the original firstVal
	int originalFirst = *firstVal;

	// pass the second val to the first one
	*firstVal = *secondVal;

	// pass the original first val to the second one
	*secondVal = originalFirst;
}

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	_getch();
	return 0;
}
